//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////

#tool "nuget:?package=NUnit.ConsoleRunner"


//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var buildNumber = Argument("buildNumber", "0");
var version = string.Format("1.0.0.{0}", buildNumber);
var outputDirectory = "./packageOutput";
var solutionName = "EventPublisher";
var solutionDirectory = "./src/";
var solutionPath = solutionDirectory + solutionName + ".sln";
var packageProjects = new []{"EventPublisher"};
var testProjects = new []{"EventPublisher.Tests"};
var repositoryUrl = "";
var repositoryApiKey = "";


//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define directories.
var buildDirectories = packageProjects.Select(p => Directory(solutionDirectory + p + "/bin") + Directory(configuration) ).ToArray();

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
	foreach(var buildDir in buildDirectories)
	{
		CleanDirectory(buildDir);
	}
    
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    NuGetRestore(solutionDirectory);
});

Task("Patch-Assembly-Info")
	.Description("Patches the AssemblyInfo files.")
	.IsDependentOn("Restore-NuGet-Packages")
	.Does(() =>
{

	var projects = packageProjects.Union(testProjects);
	foreach(var project in projects)
	{
		var file = "./src/" + project + "/Properties/AssemblyInfo.cs";
		
		CreateAssemblyInfo(file, new AssemblyInfoSettings {
			Product = solutionName,
			Version = version,
			FileVersion = version,
			InformationalVersion = (version).Trim(),
			Copyright = "2017",
			Description = solutionName,
		});

	}


});

Task("Build")
    .IsDependentOn("Patch-Assembly-Info")
    .Does(() =>
{
    if(IsRunningOnWindows())
    {
      // Use MSBuild
      MSBuild(solutionPath, settings =>
        settings.SetConfiguration(configuration)
		.WithProperty("RunOctoPack", "true")
		);
    }
    else
    {
      // Use XBuild
      XBuild(solutionPath, settings =>
        settings.SetConfiguration(configuration));
    }
});

Task("Run-Unit-Tests")
    .IsDependentOn("Build")
	.ContinueOnError()
    .Does(() =>
{
	var tests = testProjects.Select(p => solutionDirectory + p + "/bin/" + configuration + "/" + p + ".dll" ).ToArray();
	
	Information("Test assemblies{0}", tests);
    NUnit3(tests, new NUnit3Settings {
        NoResults = true
        });
});


Task("Package")
	.IsDependentOn("Run-Unit-Tests")
	.Does(() => {
		EnsureDirectoryExists(outputDirectory);
		CleanDirectory(outputDirectory);

		foreach(var project in packageProjects)
		{
			var nupkg = string.Format("{0}{1}/obj/octopacked/{1}.{2}.nupkg", solutionDirectory, project, version);
			CopyFileToDirectory(nupkg, outputDirectory);

		}

		

	});

Task("PushPackages")
	.IsDependentOn("Package")
	.Does(() => {
		if (HasEnvironmentVariable("system.ProGet_FeedUrl"))
		{
			Information("The Nuget Feed variable was present.");
			var server = EnvironmentVariable("system.ProGet_FeedUrl");
			var apikey = EnvironmentVariable("system.ProGet_APIKey");

			foreach(var project in packageProjects)
			{
				var nupkg = string.Format("{0}/{1}.{2}.nupkg", outputDirectory, project, version);
				NuGetPush(nupkg, new NuGetPushSettings {
					Source = server,
					ApiKey = apikey
				});
			}
		}
		else
		{
				Information("The Nuget Feed variable wasn't present - skipping package push.");
		}

	});


//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Run-Unit-Tests");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
