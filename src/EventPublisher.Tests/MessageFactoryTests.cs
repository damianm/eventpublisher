﻿using System;
using System.Reflection;
using EventPublisher.Tests.Fakes;
using EventPublisher.Tests.Messages;
using Nimbus.MessageContracts;
using NUnit.Framework;

namespace EventPublisher.Tests
{
    public class MessageFactoryTests
    {

        [TestFixture]
        public class when_hydrating_a_message_with_no_properties : SpecificationFor<MessageFactory>
        {
            private IBusEvent _message;

            public override MessageFactory Given()
            {

                var config = new FakeConfiguration(new Assembly[] { typeof(FooUpdated).Assembly });

                return new MessageFactory(config);
            }

            public override void When()
            {
                var se = new SystemEvent
                {
                    Entity = "Foo",
                    EntityAction = EntityAction.Updated,
                    EventDateTime = new DateTime(2014, 1, 1, 1, 1, 0),
                    EntityData = "",
                };

                _message = Subject.BuildMessage(se);
            }


            [Then]
            public void the_message_is_null()
            {
                Assert.IsNull(_message);
            }
        }


        [TestFixture]
        public class When_Finding_An_Update_Message : SpecificationFor<MessageFactory>
        {
            private IBusEvent _message;

            public override MessageFactory Given()
            {

                var config = new FakeConfiguration(new Assembly[] {typeof (FooUpdated).Assembly});
                
                return new MessageFactory(config);
            }

            public override void When()
            {
                var se = new SystemEvent
                {
                    Entity = "Foo",
                    EntityAction = EntityAction.Updated,
                    EventDateTime = new DateTime(2014, 1, 1, 1, 1, 0),
                    EntityData = "<data RaceEntryId=\"13150\" RaceId=\"101\"/>",
                };

                _message = Subject.BuildMessage(se);
            }

            [Then]
            public void the_message_is_the_correct_type()
            {
                Assert.AreEqual(typeof (FooUpdated), _message.GetType());
            }

            [Then]
            public void the_message_has_all_properties_set()
            {

                var message = (FooUpdated) _message;
                Assert.AreEqual(13150, message.RaceEntryId);

                Assert.AreEqual(101, message.RaceId);
            }


        }
    }
}