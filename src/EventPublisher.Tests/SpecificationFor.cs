﻿using NUnit.Framework;

namespace EventPublisher.Tests
{
    public abstract class SpecificationFor<T>
    {
        public T Subject { get; set; }

        public abstract T Given();

        public abstract void When();

        [SetUp]
        public void SetUp()
        {
            Subject = Given();
            When();
        }
    }

    public class ThenAttribute : TestAttribute
    {
    }
}