﻿using Nimbus.MessageContracts;

namespace EventPublisher.Tests.Messages
{
    public class FooUpdated : IBusEvent
    {
        public int RaceEntryId { get; set; }
        public int RaceId { get; set; }

         
    }
}