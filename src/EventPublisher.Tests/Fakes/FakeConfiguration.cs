﻿using System.Collections.Generic;
using System.Reflection;

namespace EventPublisher.Tests.Fakes
{
    public class FakeConfiguration : IConfiguration
    {

        public FakeConfiguration(Assembly[] assemblies)
        {
            MessageAssemblies = assemblies;
        }

        public string SqlConnectionString { get; private set; }
        public string BusConnectionString { get; private set; }
        public Assembly[] MessageAssemblies { get; private set; }
        public string ServiceName { get; private set; }
        public string ServiceDisplayName { get; private set; }
        public List<TableMonitorConfiguration> TableMonitorConfigurations { get; private set; }
    }
}