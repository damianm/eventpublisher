﻿using System.Configuration;

namespace EventPublisher.TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var eventpublisher = new EventPublisherService()
                .WithSqlConnectionString(ConfigurationManager.AppSettings["SqlConnectionString"])
                .WithBusConnectionString(ConfigurationManager.AppSettings["BusConnectionString"])
                .WithServiceName("EventPublisher", "Event Publisher")
                .WithMessageAssemblies(typeof(Program).Assembly);

            eventpublisher.MonitorTable("RaceEntry", "Yacht", "race_entry_id", new Column("race_entry_id", "RaceEntryId"), new Column("race_id", "RaceId"));

            eventpublisher.Run();
        }
    }
}