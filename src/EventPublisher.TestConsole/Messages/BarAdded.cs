﻿using Nimbus.MessageContracts;

namespace EventPublisher.Messages
{
    public class YachtUpdated : IBusEvent
    {
        public int RaceEntryId { get; set; }
        public int RaceId { get; set; }
         
    }
}