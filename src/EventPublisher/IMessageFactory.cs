﻿using Nimbus.MessageContracts;

namespace EventPublisher
{
    public interface IMessageFactory
    {
        IBusEvent BuildMessage(SystemEvent systemEvent);
    }
}