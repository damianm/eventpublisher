﻿namespace EventPublisher
{
    public class Column
    {
        private readonly string _columnName;
        private readonly string _columnAlias;

        public Column(string columnName, string columnAlias)
        {
            _columnName = columnName;
            _columnAlias = columnAlias;
        }

        public Column(string columnName)
        {
            _columnName = columnName;
            _columnAlias = ColumnName;
        }


        public string ColumnName
        {
            get { return _columnName; }
        }

        public string ColumnAlias
        {
            get { return _columnAlias; }
        }
    }
}