﻿namespace EventPublisher
{
    public interface IEventRepository
    {
        SystemEvent GetNextEvent();
        void CommitEvent(int id);
        void Initialise();
    }
}