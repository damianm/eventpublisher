﻿using System;

namespace EventPublisher
{
    public class SystemEvent
    {
        public int Id { get; set; }
        public DateTime EventDateTime { get; set; }
        public EventStatus EventStatus { get; set; }
        public string Entity { get; set; }
        public EntityAction EntityAction { get; set; }

        public string EntityData { get; set; }

    }
}