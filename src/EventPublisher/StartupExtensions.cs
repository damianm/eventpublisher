﻿using System.Reflection;

namespace EventPublisher
{
    public static class StartupExtensions
    {

        public static EventPublisherService WithSqlConnectionString(this EventPublisherService eventPublisher, string sqlConnectionString)
        {
            eventPublisher.PublisherConfiguration.SqlConnectionString = sqlConnectionString;
            return eventPublisher;
        }

        public static EventPublisherService WithBusConnectionString(this EventPublisherService eventPublisher,
            string busConnectionString)
        {
            eventPublisher.PublisherConfiguration.BusConnectionString = busConnectionString;
            return eventPublisher;
        }


        public static EventPublisherService WithMessageAssemblies(this EventPublisherService eventPublisher,
            params Assembly[] messageAssemblies)
        {
            eventPublisher.PublisherConfiguration.MessageAssemblies = messageAssemblies;
            return eventPublisher;
        }

        public static EventPublisherService WithServiceName(this EventPublisherService eventPublisher, string name, string displayName)
        {
            eventPublisher.PublisherConfiguration.ServiceName = name;
            eventPublisher.PublisherConfiguration.ServiceDisplayName = displayName;

            return eventPublisher;
        }

        public static EventPublisherService MonitorTable(this EventPublisherService eventPublisher, string tableName,
            string entityName, string key, params Column[] columns)
        {
            var config = new TableMonitorConfiguration
            {
                EntityName = entityName,
                TableName = tableName,
                Key = key,
                Columns = columns
            };

            eventPublisher.PublisherConfiguration.TableMonitorConfigurations.Add(config);

            return eventPublisher;
        }

    }
}