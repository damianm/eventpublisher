﻿using System.Collections.Generic;
using System.Reflection;

namespace EventPublisher
{
    internal class PublisherConfiguration : IConfiguration
    {
        public PublisherConfiguration()
        {
            TableMonitorConfigurations = new List<TableMonitorConfiguration>();
        }

        public string SqlConnectionString { get; set; }
        public string BusConnectionString { get; set; }

        public Assembly[] MessageAssemblies { get; set; }

        public string ServiceName {get; set;}
        public string ServiceDisplayName { get; set; }

        public List<TableMonitorConfiguration> TableMonitorConfigurations { get; set; }
 
    }
}