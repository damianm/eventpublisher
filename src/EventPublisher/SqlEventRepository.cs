﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using Dapper;
using Serilog;

namespace EventPublisher
{
    public class SqlEventRepository : IEventRepository
    {
        private readonly string _sqlConnectionString;

        public SqlEventRepository(IConfiguration configuration)
        {
            _sqlConnectionString = configuration.SqlConnectionString;
        }

        public SystemEvent GetNextEvent()
        {
            SystemEvent nextEvent = null;

            using (var ts = new TransactionScope())
            {
                using (var conn = GetConnection())
                {
                    conn.Open();

                    try
                    {
                        var sql =
                            "SELECT Top 1 Id, EventDateTime, EventStatus, Entity, EntityAction, EntityData FROM EventPublisher WHERE EventStatus = @Status ORDER BY Id";

                        nextEvent = conn.Query<SystemEvent>(sql, new { Status = EventStatus.Pending}).FirstOrDefault();
                        if (nextEvent != null)
                        {
                            sql = "UPDATE EventPublisher SET EventStatus = @EventStatus WHERE Id = @Id";
                            conn.Execute(sql, new {EventStatus = EventStatus.InProgress, nextEvent.Id});
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error(exception, "Error getting next event");
                    }
                    finally
                    {
                    }


                    conn.Close();
                }
                ts.Complete();
            }
            return nextEvent;
        }

        public void CommitEvent(int id)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var sql = "DELETE EventPublisher WHERE Id = @Id";
                conn.Execute(sql, new {Id = id});
                conn.Close();
            }
        }

        public void Initialise()
        {
            var sql =
                @"IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventPublisher]') AND TYPE IN (N'U')) BEGIN

CREATE TABLE [dbo].[EventPublisher]
(
	Id int NOT NULL IDENTITY Primary Key,
	EventDateTime DateTime NOT NULL Default(GetUTCDate()),
	EventStatus int NOT NULL DEFAULT(0),
	Entity NVarChar(200) NOT NULL,
	EntityAction int NOT NULL,
    EntityData XML

)
END

";
            using (var conn = GetConnection())
            {
                conn.Open();
                conn.Execute(sql);
                conn.Close();
            }
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(_sqlConnectionString);
        }
    }
}