﻿namespace EventPublisher
{
    public interface ITriggerBuilder
    {
        void BuildTriggers();
    }
}