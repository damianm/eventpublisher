﻿using Autofac;

namespace EventPublisher.AutofacModules
{
    public class EventPublisherModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SqlEventRepository>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<MessageFactory>().As<IMessageFactory>().InstancePerLifetimeScope();
            builder.RegisterType<TriggerBuilder>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<EventPump>().SingleInstance();

        }
    }
}