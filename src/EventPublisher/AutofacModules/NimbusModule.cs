﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Autofac;
using Nimbus;
using Nimbus.Configuration;
using Nimbus.Infrastructure;
using Nimbus.Logger.Serilog;
using Nimbus.Transports.Redis;
using Module = Autofac.Module;

namespace EventPublisher.AutofacModules
{
    public class NimbusModule : Module
    {
        private readonly IConfiguration _configuration;

        internal NimbusModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            
            var connectionString = _configuration.BusConnectionString;
            var typeProvider = new AssemblyScanningTypeProvider(_configuration.MessageAssemblies);

            builder.RegisterNimbus(typeProvider);

            builder.RegisterType<SerilogStaticLogger>()
                .AsImplementedInterfaces()
                .SingleInstance();


            builder.Register(componentContext => new BusBuilder()
                .Configure()
                .WithTransport(new RedisTransportConfiguration().WithConnectionString(connectionString))
                .WithNames(_configuration.ServiceName, Environment.MachineName)
                .WithTypesFrom(typeProvider)
                .WithAutofacDefaults(componentContext)
                .WithSerilogLogger()
                .Build())
                .As<IBus>()
                .SingleInstance();
        }
    }
}