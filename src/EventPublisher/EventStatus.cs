﻿namespace EventPublisher
{
    public enum EventStatus
    {

        Pending, 
        InProgress,
        Processed
    }
}