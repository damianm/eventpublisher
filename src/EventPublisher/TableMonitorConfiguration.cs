﻿namespace EventPublisher
{
    public class TableMonitorConfiguration
    {
        public string TableName { get; set; }
        public string EntityName { get; set; }
        public string Key { get; set; }
        public Column[] Columns { get; set; }

        
    }
}