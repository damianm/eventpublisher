﻿using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Serilog;

namespace EventPublisher
{
    internal class TriggerBuilder : ITriggerBuilder
    {
        private readonly IConfiguration _configuration;

        private readonly string[] _triggerActions = { "INSERT", "UPDATE", "DELETE" };

        public TriggerBuilder(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public void BuildTriggers()
        {
            using (var conn = new SqlConnection(_configuration.SqlConnectionString))
            {
                conn.Open();

                foreach (var table in _configuration.TableMonitorConfigurations)
                {
                    foreach (var action in _triggerActions)
                    {

                        var triggerName = string.Format("[EventPublisher_{0}_{1}]", table.TableName, action);

                        var sql = BuildDropTriggerStatement(triggerName, action);
                        Log.Debug("Executing drop trigger code {@sql}", sql);
                        conn.Execute(sql);


                        sql = BuildTriggerCreateStatement(action, triggerName, table);

                        Log.Debug("Executing create trigger code {@sql}", sql);
                        conn.Execute(sql);


                    }

                    
                }

                conn.Close();
            }
        }

        private static string BuildTriggerCreateStatement(string action, string triggerName, TableMonitorConfiguration table)
        {
            
            var sb = new StringBuilder();

            string triggerTable = "";
            var entityAction = EntityAction.Added;
            switch (action)
            {
                case "INSERT" :
                    entityAction = EntityAction.Added;
                    triggerTable = "INSERTED";
                    break;
                case "UPDATE" :
                    entityAction = EntityAction.Updated;
                    triggerTable = "INSERTED";
                    break;
                case "DELETE" :
                    entityAction = EntityAction.Deleted;
                    triggerTable = "DELETED";
                    break;
                default :
                    break;
            }

            sb.AppendFormat("CREATE TRIGGER {0} ON {1} AFTER {2}", triggerName, table.TableName, action);
            sb.AppendLine();
            sb.AppendLine("AS BEGIN");
            sb.AppendLine("SET NOCOUNT ON");
            sb.AppendLine("INSERT EventPublisher (Entity, EntityAction, EntityData)");
            sb.AppendFormat("SELECT '{0}', {1}, (", table.EntityName, (int)entityAction);

            //Select line
            var select = table.Columns.Aggregate("SELECT ", (current, column) => current + (column.ColumnName + " AS " + column.ColumnAlias + ","));

            select = select.Remove(select.Length - 1);
            sb.AppendLine(select);
            sb.AppendFormat("FROM {0} as data", triggerTable);
            sb.AppendLine();
            sb.AppendFormat("WHERE data.{0} = i.{0}", table.Key);
            sb.AppendLine();
            sb.AppendLine("FOR XML Auto");

            sb.AppendFormat(") FROM {0} i", triggerTable);
            sb.AppendLine();
            sb.AppendLine("END");

            return sb.ToString();
        }

        private static string BuildDropTriggerStatement(string triggerName, string action)
        {
            
            var sql = @"
                                IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('@Triggername') AND TYPE IN (N'TR')) BEGIN
	                                DROP TRIGGER @Triggername
                                END";
            sql = sql.Replace("@Triggername", triggerName);
            return sql;
        }
    }
}