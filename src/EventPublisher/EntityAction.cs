﻿namespace EventPublisher
{
    public enum EntityAction
    {
        Added,
        Updated,
        Deleted
    }
}