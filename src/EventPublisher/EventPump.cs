﻿using System.Timers;
using Nimbus;
using Serilog;

namespace EventPublisher
{
    public class EventPump
    {
        private readonly IEventRepository _eventRepository;
        private readonly IBus _bus;
        private readonly IMessageFactory _messageFactory;
        private readonly ITriggerBuilder _triggerBuilder;
        private Timer _timer;

        public EventPump(IEventRepository eventRepository, IBus bus, IMessageFactory messageFactory, ITriggerBuilder triggerBuilder)
        {
            _eventRepository = eventRepository;
            _bus = bus;
            _messageFactory = messageFactory;
            _triggerBuilder = triggerBuilder;
        }

        private void SetTimer()
        {
            _timer = new Timer(10000) {AutoReset = false};
            _timer.Elapsed += Poll;
            _timer.Start();

        }

        void Poll(object sender, ElapsedEventArgs e)
        {
            Log.Debug("Polling for next event");

            var nextEvent = _eventRepository.GetNextEvent();
            while (nextEvent != null)
            {
                Log.Information("Publishing : {@nextEvent}", nextEvent);
                
                //publish
                var message = _messageFactory.BuildMessage(nextEvent);
                if (message != null)
                {
                    Log.Debug("Got message {@message} for {@event}", message, nextEvent);
                    _bus.Publish(message);
                }
                else
                {
                    Log.Debug("No message for {@event}", nextEvent);
                }

                //commit
                _eventRepository.CommitEvent(nextEvent.Id);
                Log.Debug("Committed event Id {Id}", nextEvent.Id);

                nextEvent = _eventRepository.GetNextEvent();
            }

            SetTimer();
        }

        public void Start()
        {
            _eventRepository.Initialise();
            _triggerBuilder.BuildTriggers();

            Log.Information("Starting Poller");
            SetTimer();
        }
        
        public void Stop()
        {
            Log.Information("Stopping Poller");
            _timer.Stop();
        }
    }
}