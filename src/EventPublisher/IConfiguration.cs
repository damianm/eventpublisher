﻿using System.Collections.Generic;
using System.Reflection;

namespace EventPublisher
{
    public interface IConfiguration
    {
        string SqlConnectionString { get; }
        string BusConnectionString { get; }
        Assembly[] MessageAssemblies { get; }

        string ServiceName { get; }
        string ServiceDisplayName { get; }

        List<TableMonitorConfiguration> TableMonitorConfigurations { get; }

    }
}