﻿using System;
using Autofac;
using EventPublisher.AutofacModules;
using Serilog;
using Topshelf;

namespace EventPublisher
{
    public class EventPublisherService
    {
        internal PublisherConfiguration PublisherConfiguration;

        public EventPublisherService()
        {
            PublisherConfiguration = new PublisherConfiguration();
        }

        public void Run()
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("App", "EventPublisher")
                .Enrich.WithProperty("Host", Environment.MachineName)
                .MinimumLevel.Debug()
                .ReadFrom.AppSettings()
                .WriteTo.ColoredConsole()
                .CreateLogger();


            var containerBuilder = new ContainerBuilder();


            containerBuilder.Register(c => PublisherConfiguration).As<IConfiguration>().SingleInstance();

            containerBuilder.RegisterModule(new NimbusModule(PublisherConfiguration));
            containerBuilder.RegisterModule(new EventPublisherModule());


            IContainer container = containerBuilder.Build();

            HostFactory.Run(x =>
            {
                x.Service<EventPump>(s =>
                {
                    s.ConstructUsing(name => container.Resolve<EventPump>());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.UseSerilog();
                x.RunAsLocalSystem();
                x.SetDescription(PublisherConfiguration.ServiceDisplayName);
                x.SetDisplayName(PublisherConfiguration.ServiceDisplayName);
                x.SetServiceName(PublisherConfiguration.ServiceName);
            });
        }
    }
}