﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using Nimbus.MessageContracts;

namespace EventPublisher
{
    public class MessageFactory : IMessageFactory
    {
        private readonly IConfiguration _configuration;

        private readonly Dictionary<string, Type> _knownTypes = new Dictionary<string, Type>();

        public MessageFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IBusEvent BuildMessage(SystemEvent systemEvent)
        {
            IBusEvent message = null;

            var messageType = FindType(systemEvent);

            if (messageType != null && !string.IsNullOrEmpty(systemEvent.EntityData))
            {
                message = InstantiateMessage(messageType, systemEvent);
            }

            return message;
        }

        private IBusEvent InstantiateMessage(Type messageType, SystemEvent systemEvent)
        {
            var message = Activator.CreateInstance(messageType);


            var properties = GetPropertyBag(systemEvent);
            foreach (var property in properties)
            {
                SetProperty(message, property.Key, property.Value);
            }

            return (IBusEvent)message;

        }

        private Dictionary<string,string> GetPropertyBag(SystemEvent systemEvent)
        {
            var bag = new Dictionary<string, string>();
            var xml = new XmlDocument();
            xml.LoadXml(systemEvent.EntityData);
            if (xml.DocumentElement == null)
                return bag;
            
            foreach (XmlAttribute att in xml.DocumentElement.Attributes)
            {
                bag.Add(att.Name, att.Value);
            }

            return bag;
        }

        private void SetProperty(object message, string name, object value)
        {
            var prop = message.GetType()
                .GetProperty(name, BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.Public);
            if (prop != null && prop.CanWrite)
            {
                var newValue = Convert.ChangeType(value, prop.PropertyType);
                prop.SetValue(message, newValue);
            }

        }

        private Type FindType(SystemEvent systemEvent)
        {
            if (_knownTypes.ContainsKey(GetCacheKey(systemEvent)))
                return _knownTypes[GetCacheKey(systemEvent)];


            
            foreach (var assembly in _configuration.MessageAssemblies)
            {

                var reflectedType =
                    assembly.GetTypes()
                        .FirstOrDefault(t => IsNamedCorrectly(t, systemEvent) && typeof(IBusEvent).IsAssignableFrom(t));

                if (reflectedType != null)
                {
                    _knownTypes[GetCacheKey(systemEvent)] = reflectedType;
                    return reflectedType;
                }
            }
            _knownTypes[GetCacheKey(systemEvent)] = null;
            return null;
        }

        private string GetCacheKey(SystemEvent systemEvent)
        {
            return (systemEvent.Entity + systemEvent.EntityAction).ToLower();
        }

        private bool IsNamedCorrectly(Type type, SystemEvent systemEvent)
        {
            return type.Name.ToLower() == GetCacheKey(systemEvent);
        }
    }
}